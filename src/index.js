import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';

import Keycloak from 'keycloak-js';

// добавить объект конфига из keycloak
export const kc = Keycloak({
    url: 'https:yorlink.com/auth/',
    realm: 'myRealm',
    clientId: 'react-ui',
});

console.log(kc);

const updateLocalStorage = () => {
    localStorage.setItem('kc_token', kc.token);
    localStorage.setItem('kc_refreshToken', kc.refreshToken);
};

kc.init({onLoad: 'login-required'})
    .then((authenticated) => {
        if (authenticated) {
            updateLocalStorage();

            setInterval(() => {
                kc.updateToken(11)
                    .then((refreshed) => {
                        if (refreshed) {
                            updateLocalStorage();
                        } else {
                        }
                    })
                    .catch(() => kc.logout());
            }, 10000);

            ReactDOM.render(
                <React.StrictMode>
                    <App />
                </React.StrictMode>,
                document.getElementById('root')
            );
        } else {
            kc.login();
        }
    })
    .catch(() => {
        kc.login();
    });

kc.onTokenExpired = () => {
    kc.logout();
    console.log('Token Expired');
};



// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
